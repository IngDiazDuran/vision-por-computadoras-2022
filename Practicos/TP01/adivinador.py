import random

def adivina(intentos_max):
    numero = random.randint(0,100)
    intentos = 0
    print("Este juego consiste en adivinar un numero entero aleatorio entre 0 y 100.")
    print("Usted tendra {} intentos para poder resolverlo.".format(intentos_max))

    while(intentos < intentos_max):
        num = input("Ingrese su numero para intentar: ")
        intentos += 1
        if num == numero:
            print("Felicitaciones! Ha adivinado el numero en {} intentos".format(intentos))
            break
    else:
        print("No ha adivinado en la cantidad de intentos maxima ({}). Mejor dediquese a otra cosa.".format(intentos))


adivina(100)
        
        
    
    





