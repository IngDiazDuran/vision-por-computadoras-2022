#!/usr/bin/env python
#-*-coding:utf-8-*-
import cv2
import numpy as np

one_line = img = cv2.imread('hoja.png', 0)
row_i = col_i = 0
# Para resolverlo podemos usar dos for anidados
for row in img:
    col_i = 0
    for col in row:
        if (img[row_i, col_i] < 200):
            img[row_i, col_i] = 0
        else:
            img[row_i, col_i] = 255
        col_i += 1
    row_i += 1

one_line[one_line < 200] = 0

cv2.imwrite('resultado.png', img)
cv2.imwrite('resultado_one_line.png', one_line)
cv2.imshow('image', img)
cv2.waitKey(0)
