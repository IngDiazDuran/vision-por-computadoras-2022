#! /usr/bin/env python
# -*- coding: utf-8 -*-
import cv2
import numpy as np

drawing = False      # true if mouse is pressed
ix, iy = -1, -1
done = False
first = True
img_file = 'hoja.png'

def draw_rect(event, x, y, flags, param):
    global ix, iy, drawing, done, fx, fy
    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        ix, iy = x, y
    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing is True :
            cv2.rectangle(img, (ix,iy), (x,y), (0,255,0), 1)
            cv2.imshow('Image', img)
    elif event == cv2 .EVENT_LBUTTONUP:
        drawing = False
        cv2.rectangle(img, (ix,iy), (x,y), (0,255,0), 1)
        fx = x
        fy = y
        done = True

img = cv2.imread(img_file)
cv2.namedWindow('Image', cv2.WINDOW_AUTOSIZE)
cv2.setMouseCallback('Image', draw_rect)

while(1):
    k = cv2.waitKey(1) & 0xFF
    if k == ord ('q'):
        break
    elif k == ord ('r'):
        img2 = cv2.imread(img_file)
        img = img2
        ready = False
    elif k == ord ('g'):
        if done is True:
            if iy == fy or ix == fx:
                continue
            if ix > fx:
                ix, fx = fx, ix
            if iy > fy:
                iy, fy = fy, iy

            clip = img[iy+1:fy,ix+1:fx,:]
            cv2.imwrite('recorte.png', clip)
            cv2.namedWindow('Imagen recortada', cv2.WINDOW_AUTOSIZE)
            cv2.imshow('Imagen recortada', clip)

    if drawing is True:
        img2 = cv2.imread(img_file)
        img =  img2
        first = True
    else:
        if first is True:
            cv2.imshow('Image', img) 


cv2.destroyAllWindows()
