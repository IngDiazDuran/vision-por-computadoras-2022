#! /usr/bin/env python
# -*- coding: utf-8 -*-
import cv2
import numpy as np
import math

drawing = False      # true if mouse is pressed
ix, iy = -1, -1
done = False
first = True
img_file = 'hoja.png'

def trans_euclidiana(angle, tx, ty, img):
    angle = math.radians(angle)
    m00 = m11 = math.cos(angle)
    m01 = math.sin(angle)
    m10 = -m01
    m02 = tx
    m12 = ty

    M = np.float32([[m00, m01, m02], [m10, m11, m12]])
    (w, h, p) = np.shape(img)
    img_trans = cv2.warpAffine(img, M, (w,h))
    return img_trans



def draw(event, x, y, flags, param):
    global ix, iy, drawing, done, fx, fy
    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        ix, iy = x, y
    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing is True :
            cv2.rectangle(img, (ix,iy), (x,y), (0,255,0), 1)
            cv2.imshow('Image', img)
    elif event == cv2 .EVENT_LBUTTONUP:
        drawing = False
        cv2.rectangle(img, (ix,iy), (x,y), (0,255,0), 1)
        fx = x
        fy = y
        done = True

img = cv2.imread(img_file)
angle= int(input('Ingrese el "angulo": '))
tx= int(input('Ingrese el "px": '))
ty= int(input('Ingrese el "py": '))


cv2.namedWindow('Image', cv2.WINDOW_AUTOSIZE)
cv2.setMouseCallback('Image', draw)

while(1):
    k = cv2.waitKey(1) & 0xFF
    if k == ord ('q'):
        break
    elif k == ord ('r'):
        img2 = cv2.imread(img_file)
        img = img2
        ready = False
    elif k == ord ('e'):
        if done is True:
            if iy == fy or ix == fx:
                continue
            if ix > fx:
                ix, fx = fx, ix
            if iy > fy:
                iy, fy = fy, iy

            clip = img[iy+1:fy,ix+1:fx,:]
            cv2.imwrite('recorte.png', clip)
            cv2.namedWindow('Imagen recortada', cv2.WINDOW_AUTOSIZE)
            cv2.imshow('Imagen recortada', clip)
            trans = trans_euclidiana(angle, tx, ty, clip)
            cv2.imwrite('trans.png', trans)
            cv2.namedWindow('Imagen rotada y trasladada', cv2.WINDOW_AUTOSIZE)
            cv2.imshow('Imagen rotada y trasladada', trans)

    if drawing is True:
        img2 = cv2.imread(img_file)
        img =  img2
        first = True
    else:
        if first is True:
            cv2.imshow('Image', img) 


cv2.destroyAllWindows()
