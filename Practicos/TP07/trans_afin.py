#! /usr/bin/env python
# -*- coding: utf-8 -*-
import cv2
import numpy as np

count = 0           # amount of points taken
points1 = [[-1, -1], [-1, -1], [-1, -1]]
points2 = [[-1, -1], [-1, -1], [-1, -1]]
img1_file = 'hoja.png'
img2_file = 'gusano.png'

def trans_afin(img):
    h, w = img.shape[:2]
    pts1 = np.float32([[0,0], [w, 0], [w,h]])
    pts2 = np.float32([[points2[0]], [points2[1]], [points2[2]]])
    M = cv2.getAffineTransform(pts1, pts2)
    img_trans = cv2.warpAffine(img, M, (w,h))
    return img_trans

def select_points(event, x, y, flags, param):
    global points2, count, draw
    if event == cv2.EVENT_LBUTTONUP:
        if (count == 0):
            points2[count] = x, y
            count = count + 1
            #cv2.circle(img_p, (x,y), radius=0, color=(0, 0, 255), thickness=-1)
            cv2.circle(img_p, (x,y), 2, (0, 0, 255), -1)
        elif (count == 1):
            if (points2[0][0] != x and points2[0][1] != y):
                points2[count] = x, y
                cv2.circle(img_p, (x,y), 2, (0, 0, 255), -1)
                count = count + 1
        elif (count == 2):
            if (points2[0][0] != x and points2[0][1] != y and points2[1][0] != x and points2[1][1] != y):
                points2[count] = x, y
                cv2.circle(img_p, (x,y), 2, (0, 0, 255), -1)
                count = 0;


img1 = cv2.imread(img1_file)
img2 = cv2.imread(img2_file)

(h, w) = img1.shape[:2]
img2 = cv2.resize(img2, (w, h))
        
img_p = img1.copy() #imagen auxiliar para que los puntos dibujados cuando clickeo no formen parte de la imagen final
cv2.namedWindow('Original')
cv2.imshow('Original', img_p)

cv2.setMouseCallback('Original', select_points)

while(1):
    cv2.imshow('Original', img_p)
    k = cv2.waitKey(1) & 0xFF
    if k == ord ('q'):
        break
    elif k == ord ('r'):
        img2 = cv2.imread(img_file)
        img = img2
        ready = False
    elif k == ord ('a'):
        img_transf = trans_afin(img2)

        #hago una mascara para poder poner la segunda imagen

        mask = img_transf.copy()
        mask[mask != 0] = 255
        mask = cv2.bitwise_not(mask)
        mask = cv2.bitwise_and(mask, img1)

        final = cv2.bitwise_or(mask, img_transf)
        cv2.imwrite('Resultado.jpg', final)
        cv2.namedWindow('Resultado')
        cv2.imshow('Resultado', final)

        img_p = img1.copy() #imagen auxiliar para que los puntos dibujados cuando clickeo no formen parte de la imagen final


cv2.destroyAllWindows()
