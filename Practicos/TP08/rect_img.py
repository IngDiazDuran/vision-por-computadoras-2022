# Este py es una resolución básica, que no corrobora la no colinealidad de
# los 4 puntos. En un primer acercamiento chequeaba que no compartan valores
# en x e y, pero en realidad eso hace que dos puntos no sean colineales con
# respecto a los ejes, y no es la consigna. Se deja esta validación en el
# código a modo de ejercicio mental.
#! /usr/bin/env python
# -*- coding: utf-8 -*-
import cv2
import numpy as np

count = 0           # amount of points taken
points = [[-1, -1], [-1, -1], [-1, -1], [-1, -1]]
img1_file = 'img_persp.jpg'
RESIZE = 500

def ResizeWithAspectRatio(image, width=None, height=None, inter=cv2.INTER_AREA): # funcion para redimensionar manteniendo ratio
    dim = None
    (h, w) = image.shape[:2]

    if width is None and height is None:
        return image
    if width is None:
        r = height / float(h)
        dim = (int(w * r), height)
    else:
        r = width / float(w)
        dim = (width, int(h * r))

    return cv2.resize(image, dim, interpolation=inter)

def rect_img(img):
    width_1 = np.sqrt(((points[0][0] - points[3][0]) ** 2) + ((points[0][1] - points[3][1]) ** 2))
    width_2 = np.sqrt(((points[1][0] - points[2][0]) ** 2) + ((points[1][1] - points[2][1]) ** 2))
    maxWidth = max(int(width_1), int(width_2))

    height_1 = np.sqrt(((points[0][0] - points[1][0]) ** 2) + ((points[0][1] - points[1][1]) ** 2))
    height_2 = np.sqrt(((points[2][0] - points[3][0]) ** 2) + ((points[1][1] - points[2][1]) ** 2))
    maxHeight = max(int(height_1), int(height_2))
    
    input_pts = np.float32([points[0], points[1], points[2], points[3]])
    output_pts = np.float32([[0,0], [0,maxHeight-1], [maxWidth-1, maxHeight-1], [maxWidth -1, 0]])
    M = cv2.getPerspectiveTransform(input_pts, output_pts)
    
    img_rect = cv2.warpPerspective(img, M, (maxWidth,maxHeight), flags=cv2.INTER_LINEAR)

    if (maxHeight > maxWidth):
        if (maxHeight < 100):
            img_rect = ResizeWithAspectRatio(img_rect, height=200)
        else:
            img_rect = ResizeWithAspectRatio(img_rect, height=500)
    else:
        if (maxWidth < 100):
            img_rect = ResizeWithAspectRatio(img_rect, width=200)
        else:
            img_rect = ResizeWithAspectRatio(img_rect, width=500)

    return img_rect

def select_points(event, x, y, flags, param):
    global points, count, draw
    if event == cv2.EVENT_LBUTTONUP:
        if (count == 0):
            points[count] = x, y
            count = count + 1
            cv2.circle(img_p, (x,y), 3, (0, 0, 255), -1)
        elif (count == 1):
            if (points[0][0] != x and points[0][1] != y):
                points[count] = x, y
                cv2.circle(img_p, (x,y), 3, (0, 0, 255), -1)
                count = count + 1
        elif (count == 2):
            if (points[0][0] != x and points[0][1] != y and points[1][0] != x and points[1][1] != y):
                points[count] = x, y
                cv2.circle(img_p, (x,y), 3, (0, 0, 255), -1)
                count = count + 1
        elif (count == 3):
            if (points[0][0] != x and points[0][1] != y and points[1][0] != x and points[1][1] != y and points[2][0] != x and points[2][1] != y):
                points[count] = x, y
                cv2.circle(img_p, (x,y), 3, (0, 0, 255), -1)
                count = 4;


img1 = cv2.imread(img1_file)
img1 = ResizeWithAspectRatio(img1, height=650)

img_p = img1.copy() #imagen auxiliar para que los puntos dibujados cuando clickeo no formen parte de la imagen final
cv2.namedWindow("Original")
cv2.imshow("Original", img_p)

while(1):
    cv2.imshow("Original", img_p)
    k = cv2.waitKey(1) & 0xFF
    if k == ord ('q'):
        break
    elif k == ord ('r'):
        cv2. destroyWindow("Resultado")
        img_p = img1.copy()
    elif k == ord ('h'):
        cv2.setMouseCallback("Original", select_points)
    elif (count == 4):
        img_rect = rect_img(img1)
        cv2.imwrite('Resultado.jpg', img_rect)
        cv2.namedWindow("Resultado")
        cv2.imshow("Resultado", img_rect)
        count = 0

cv2.destroyAllWindows()
