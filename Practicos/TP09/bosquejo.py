#! /usr/bin/env python
# -*- coding: utf-8 -*-
import cv2
import numpy as np

count = 0           # amount of points taken
points = [[700, 314], [697, 453], [810, 461], [811, 314]]
points2 = [[700, 314], [700, 413], [770, 413], [770, 314]]
img1_file = 'img_persp.jpeg'
ancho = 210
alto = 297
escala = 3

def ResizeWithAspectRatio(image, width=None, height=None, inter=cv2.INTER_AREA): # funcion para redimensionar manteniendo ratio
    dim = None
    (h, w) = image.shape[:2]

    if width is None and height is None:
        return image
    if width is None:
        r = height / float(h)
        dim = (int(w * r), height)
    else:
        r = width / float(w)
        dim = (width, int(h * r))

    return cv2.resize(image, dim, interpolation=inter)


def rect_img(img):
    global points, ancho, alto, escala
    img_h,img_w = img.shape[:2]

    input_pts = np.float32([points[0], points[1], points[2], points[3]])
    output_pts = np.float32([points[0], [points[0][0],points[0][1]+alto/escala], [points[0][0]+ancho/escala,points[0][1]+alto/escala], [points[0][0]+ancho/escala,points[0][1]]])
    M = cv2.getPerspectiveTransform(input_pts, output_pts)

    img_rect = cv2.warpPerspective(img, M, (img_w,img_h))

    return img_rect

def select_points(event, x, y, flags, param):
    global points, count, draw
    if event == cv2.EVENT_LBUTTONUP:
        if (count == 0):
            points[count] = x, y
            count = count + 1
            cv2.circle(img_p, (x,y), 3, (0, 0, 255), -1)
        elif (count == 1):
            points[count] = x, y
            cv2.circle(img_p, (x,y), 3, (0, 0, 255), -1)
            count = count + 1
        elif (count == 2):
            points[count] = x, y
            cv2.circle(img_p, (x,y), 3, (0, 0, 255), -1)
            count = count + 1
        elif (count == 3):
            pendiente = (points[0][1] - points[1][1]) / (points[0][0] - points[1][0])
            ordenada_origen = points[0][1] - pendiente*points[0][0]
            if (points[2][1] != pendiente*points[2][0] + ordenada_origen):
                points[count] = x, y
                cv2.circle(img_p, (x,y), 3, (0, 0, 255), -1)
                count = 4;
            elif (y != pendiente*x + ordenada_origen):
                points[count] = x, y
                cv2.circle(img_p, (x,y), 3, (0, 0, 255), -1)
                count = 4;


img1 = cv2.imread(img1_file)
img1 = ResizeWithAspectRatio(img1, height=650)

img_p = img1.copy() #imagen auxiliar para que los puntos dibujados cuando clickeo no formen parte de la imagen final
cv2.namedWindow("Original")
cv2.imshow("Original", img_p)

while(1):
    cv2.imshow("Original", img_p)
    k = cv2.waitKey(1) & 0xFF
    if k == ord ('q'):
        break
    elif k == ord ('r'):
        points = [[700, 314], [697, 453], [810, 461], [811, 314]]
        points2 = [[700, 314], [700, 413], [770, 413], [770, 314]]
        img1_file = 'img_persp.jpeg'
        ancho = 210
        alto = 297
        escala = 3
    elif k == ord ('m'):

        cv2. destroyWindow("Transformada")
        cv2. destroyWindow("Original")
        img_p = img1.copy()
        escala = int(input("Definir la escala a utilizar [mm/pixeles] (recomendación 3):"))
        ancho = int(input("Definir el ancho del rectangulo de referencia [mm]:"))
        alto = int(input("Definir el ancho del rectangulo de referencia [mm]:"))
        cv2.imshow("Original", img_p)
        cv2.putText(img_p, "Seleccionar puntos de perspectiva:" , (0,14), cv2.FONT_HERSHEY_PLAIN, 1.2, (0,0,255), 1,cv2.LINE_AA)
        cv2.putText(img_p, "(sentido antihorario, comenzando por esquina superior izquierda)" , (0,14+18), cv2.FONT_HERSHEY_PLAIN, 1.2, (0,0,255), 1,cv2.LINE_AA)
        cv2.setMouseCallback("Original", select_points)
    elif (count == 4):
        img_rect = rect_img(img1)
        cv2.imwrite('Transformada.jpg', img_rect)
        cv2.namedWindow("Transformada")
        cv2.imshow("Transformada", img_rect)
        count = 0

cv2.destroyAllWindows()
