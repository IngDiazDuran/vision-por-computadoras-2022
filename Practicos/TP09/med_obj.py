#! /usr/bin/env python
# -*- coding: utf-8 -*-
import cv2
import numpy as np
import math

count = 0           # amount of points taken
count_m = 0           # amount of points taken for a measuremen
points_m = [[-1,-1], [-1,-1]]
img1_file = 'img_persp.jpeg'

ancho = 210
alto = 297
escala = 3.4
offset_x=50
offset_y=20
points = [[700, 314], [697, 453], [810, 461], [811, 314]]
points2 = [[700+offset_x, 314+offset_y], [700+offset_x, int(points[0][1]+alto/escala)+offset_y], [int(points[0][0]+ancho/escala)+offset_x, int(points[0][1]+alto/escala)+offset_y], [int(points[0][0]+ancho/escala)+offset_x, 314+offset_y]]
# Esta forma es matemáticamente más correcta, pero no entra en la imagen
#escala_y=alto/math.dist(points[0], points[1])
#escala_xancho/math.dist(points[0], points[2])
#points2 = [[700+offset_x, 314+offset_y], [700+offset_x, int(points[0][1]+alto/escala_y)+offset_y], [int(points[0][0]+ancho/escala_x)+offset_x, int(points[0][1]+alto/escala_y)+offset_y], [int(points[0][0]+ancho/escala_x)+offset_x, 314+offset_y]]


def ResizeWithAspectRatio(image, width=None, height=None, inter=cv2.INTER_AREA): # funcion para redimensionar manteniendo ratio
    dim = None
    (h, w) = image.shape[:2]

    if width is None and height is None:
        return image
    if width is None:
        r = height / float(h)
        dim = (int(w * r), height)
    else:
        r = width / float(w)
        dim = (width, int(h * r))

    return cv2.resize(image, dim, interpolation=inter)

def choose_ref():
    global img_p, escala, ancho, alto, offset_x, offset_
    cv2.destroyWindow("Transformada")
    cv2.destroyWindow("Original")
    img_p = img1.copy()
    escala = float(input("Definir la escala a utilizar [mm/pixeles] (recomendación 3.4): "))
    ancho = int(input("Definir el ancho del rectangulo de referencia [mm] (recomendación 210): "))
    alto = int(input("Definir el ancho del rectangulo de referencia [mm] (recomendación 297): "))
    offset_x = int(input("Definir el offset en x [pixeles] (recomendación 50): "))
    offset_y = int(input("Definir el offset en y [pixeles] (recomendación 20): "))
    cv2.namedWindow("Original")
    cv2.imshow("Original", img_p)
    cv2.putText(img_p, "Seleccionar puntos de perspectiva:" , (0,14), cv2.FONT_HERSHEY_PLAIN, 1.2, (0,0,255), 1,cv2.LINE_AA)
    cv2.putText(img_p, "(sentido antihorario, comenzando por esquina superior izquierda)" , (0,14+18), cv2.FONT_HERSHEY_PLAIN, 1.2, (0,0,255), 1,cv2.LINE_AA)
    cv2.setMouseCallback("Original", select_points)


def rect_img(img):
    global points, ancho, alto, escala
    img_h,img_w = img.shape[:2]

    input_pts = np.float32([points[0], points[1], points[2], points[3]])
    output_pts = np.float32([points2[0], points2[1], points2[2], points2[3]])
    M = cv2.getPerspectiveTransform(input_pts, output_pts)

    img_rect = cv2.warpPerspective(img, M, (img_w,img_h))
    cv2.circle(img_rect, (points2[0]), 3, (0, 0, 255), -1)
    cv2.circle(img_rect, (points2[1]), 3, (0, 0, 255), -1)
    cv2.circle(img_rect, (points2[2]), 3, (0, 0, 255), -1)
    cv2.circle(img_rect, (points2[3]), 3, (0, 0, 255), -1)
    cv2.line(img_rect,points2[0],points2[1],(255,255,0),1)
    textsize = cv2.getTextSize("h=%dmm" % alto, cv2.FONT_HERSHEY_DUPLEX, 0.4, 2)[0]
    cv2.putText(img_rect, f"h=%dmm" % (alto), (int(min(points2[0][0],points2[1][0])+abs(points2[0][0]-points2[1][0])//2-textsize[0]),int(min(points2[0][1],points2[1][1])+abs(points2[0][1]-points2[1][1])//2)), cv2.FONT_HERSHEY_DUPLEX, 0.4, (255,255,0))
    cv2.line(img_rect,points2[0],points2[3],(255,255,0),1)
    textsize = cv2.getTextSize("w=%dmm" % ancho, cv2.FONT_HERSHEY_DUPLEX, 0.4, 2)[0]
    cv2.putText(img_rect, f"w=%dmm" % ancho, (int(min(points2[0][0],points2[3][0])+abs(points2[0][0]-points2[3][0])//2-textsize[0]//2),int(min(points2[0][1],points2[3][1])+abs(points2[0][1]-points2[3][1])//2-textsize[1]//2)), cv2.FONT_HERSHEY_DUPLEX, 0.4, (255,255,0))

    return img_rect


def select_points(event, x, y, flags, param):
    global points, count, draw, img_p
    cv2.imshow("Original", img_p)
    if event == cv2.EVENT_LBUTTONUP:
        if (count == 0):
            points[count] = x, y
            count = count + 1
            cv2.circle(img_p, (x,y), 3, (0, 0, 255), -1)
        elif (count == 1):
            points[count] = x, y
            cv2.circle(img_p, (x,y), 3, (0, 0, 255), -1)
            count = count + 1
        elif (count == 2):
            points[count] = x, y
            cv2.circle(img_p, (x,y), 3, (0, 0, 255), -1)
            count = count + 1
        elif (count == 3):
            pendiente = (points[0][1] - points[1][1]) / (points[0][0] - points[1][0])
            ordenada_origen = points[0][1] - pendiente*points[0][0]
            if (points[2][1] != pendiente*points[2][0] + ordenada_origen):
                points[count] = x, y
                cv2.circle(img_p, (x,y), 3, (0, 0, 255), -1)
                count = 4;
            elif (y != pendiente*x + ordenada_origen):
                points[count] = x, y
                cv2.circle(img_p, (x,y), 3, (0, 0, 255), -1)
                count = 4;

def measure(event, x, y, flags, param):
    global count_m, measure, img_m
    cv2.imshow("Transformada", img_m)
    if event == cv2.EVENT_LBUTTONUP:
        if (count_m == 0):
            points_m[count_m] = x, y
            count_m = count_m + 1
            cv2.circle(img_m, (x,y), 3, (0, 0, 255), -1)
        elif (count_m == 1):
            points_m[count_m] = x, y
            cv2.circle(img_m, (x,y), 3, (0, 0, 255), -1)
            count_m = count_m + 1


img1 = cv2.imread(img1_file)
img1 = ResizeWithAspectRatio(img1, height=650)

img_p = img1.copy() #imagen auxiliar para que los puntos dibujados cuando clickeo no formen parte de la imagen final
cv2.namedWindow("Original")
cv2.imshow("Original", img_p)

img_rect = rect_img(img1)
cv2.namedWindow("Transformada")
cv2.imshow("Transformada", img_rect)

while(1):
    #cv2.imshow("Transformada", img_rect)
    k = cv2.waitKey(1) & 0xFF
    if k == ord ('q'):
        break
    elif k == ord ('r'):
        cv2. destroyWindow("Transformada")
        cv2. destroyWindow("Original")
        img_p = img1.copy()
        ancho = 210
        alto = 297
        escala = 3.4
        offset_x=50
        offset_y=20
        points = [[700, 314], [697, 453], [810, 461], [811, 314]]
        points2 = [[700+offset_x, 314+offset_y], [700+offset_x, int(points[0][1]+alto/escala)+offset_y], [int(points[0][0]+ancho/escala)+offset_x, int(points[0][1]+alto/escala)+offset_y], [int(points[0][0]+ancho/escala)+offset_x, 314+offset_y]]
        cv2.namedWindow("Original")
        cv2.imshow("Original", img_p)
        
        img_rect = rect_img(img1)
        cv2.namedWindow("Transformada")
        cv2.imshow("Transformada", img_rect)
    elif k == ord ('c'):
        choose_ref()
    elif (count == 4):
        points2 = [[points[0][0]+offset_x, points[0][1]+offset_y], [points[0][0]+offset_x, int(points[0][1]+alto/escala)+offset_y], [int(points[0][0]+ancho/escala)+offset_x, int(points[0][1]+alto/escala)+offset_y], [int(points[0][0]+ancho/escala)+offset_x, points[0][1]+offset_y]]
        img_rect = rect_img(img1)
        cv2.namedWindow("Transformada")
        cv2.imshow("Transformada", img_rect)
        count = 0
    elif k == ord ('m'):
        img_m = img_rect.copy()
        cv2.imshow("Transformada", img_m)
        cv2.putText(img_m, "Seleccionar puntos para medir" , (0,14), cv2.FONT_HERSHEY_PLAIN, 1.2, (0,0,255), 1,cv2.LINE_AA)
        cv2.setMouseCallback("Transformada", measure)
    elif (count_m == 2):
        distancia = math.dist(points_m[0], points_m[1])*escala
        cv2.line(img_m,points_m[0],points_m[1],(255,255,0),1)
        cv2.putText(img_m, f"%dmm" % distancia, (int(min(points_m[0][0],points_m[1][0])+abs(points_m[0][0]-points_m[1][0])//2),int(min(points_m[0][1],points_m[1][1])+abs(points_m[0][1]-points_m[1][1])//2)), cv2.FONT_HERSHEY_DUPLEX, 0.4, (255,255,0))
        count_m = 0



cv2.destroyAllWindows()
