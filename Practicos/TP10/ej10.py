'''
Sample Usage:-
python3 ej10.py --K_Matrix repo/calibration_matrix.npy --D_Coeff repo/distortion_coefficients.npy --type DICT_5X5_100
'''


# importing the modules
import numpy as np
import cv2
import sys
from repo.utils import ARUCO_DICT
import argparse
import time

# set Width and Height of output Screen
frameWidth = 640
frameHeight = 480

# capturing Video from Webcam
#cap = cv2.VideoCapture(1)
#cap.set(3, frameWidth)
#cap.set(4, frameHeight)

# set brightness, id is 10 and
# value can be changed accordingly
#cap.set(10,150)

# [x , y , colorId ]
myPoints = []
draw = False
blue=255
green=255
red=255

def markerCenter(frame, aruco_dict_type, matrix_coefficients, distortion_coefficients):
    newPoints = []

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    cv2.aruco_dict = cv2.aruco.Dictionary_get(aruco_dict_type)
    parameters = cv2.aruco.DetectorParameters_create()


    corners, ids, rejected_img_points = cv2.aruco.detectMarkers(gray, cv2.aruco_dict,parameters=parameters)
        #cameraMatrix=matrix_coefficients,
        #distCoeff=distortion_coefficients)

        # If markers are detected
    if len(corners) > 0:
        for i in range(0, len(ids)):
            # Estimate pose of each marker and return the values rvec and tvec---(different from those of camera coefficients)
            rvec, tvec, markerPoints = cv2.aruco.estimatePoseSingleMarkers(corners[i], 0.02, matrix_coefficients,
                                                                       distortion_coefficients)

            #print(tvec[0, 1])
            #x=tvec[0][0]
            #y=tvec[0][1]
            #x = '{:.2f}'.format(tvec[0])
            #y = '{:.2f}'.format(tvec[1])
            x_sum = corners[0][0][0][0]+ corners[0][0][1][0]+ corners[0][0][2][0]+ corners[0][0][3][0]
            y_sum = corners[0][0][0][1]+ corners[0][0][1][1]+ corners[0][0][2][1]+ corners[0][0][3][1]
            
            x = x_sum*.25
            y = y_sum*.25

            if x != 0 and y != 0:
                newPoints.append([x,y])


    return newPoints


# draws your action on virtual canvas
def drawOnCanvas(myPoints):
    for point in myPoints:
        cv2.circle(imgResult, (int(point[0]), int(point[1])),
                   10, point[2], cv2.FILLED)


if __name__ == '__main__':

    ap = argparse.ArgumentParser()
    ap.add_argument("-k", "--K_Matrix", required=True, help="Path to calibration matrix (numpy file)")
    ap.add_argument("-d", "--D_Coeff", required=True, help="Path to distortion coefficients (numpy file)")
    ap.add_argument("-t", "--type", type=str, default="DICT_ARUCO_ORIGINAL", help="Type of ArUCo tag to detect")
    args = vars(ap.parse_args())

    
    if ARUCO_DICT.get(args["type"], None) is None:
        print(f"ArUCo tag type '{args['type']}' is not supported")
        sys.exit(0)

    aruco_dict_type = ARUCO_DICT[args["type"]]
    calibration_matrix_path = args["K_Matrix"]
    distortion_coefficients_path = args["D_Coeff"]
    
    k = np.load(calibration_matrix_path)
    d = np.load(distortion_coefficients_path)

    video = cv2.VideoCapture(1)
    time.sleep(2.0)

    # running infinite while loop so that
    # program keep running until we close it
    while True:
        ret, frame = video.read()
        imgResult = frame.copy()

        if not ret:
            break
        
        # finding the center for the points
        newPoints = markerCenter(frame, aruco_dict_type, k, d)
        if len(newPoints)!= 0 and draw == True:
            for newP in newPoints:
                newP.append(colour)
                myPoints.append(newP)
        if len(myPoints)!= 0:
    
            # drawing the points
            drawOnCanvas(myPoints)
    
        # displaying output on Screen
        cv2.imshow("Result", imgResult)

        colour=(blue,green,red)
        key = cv2.waitKey(1) & 0xFF
        if key == ord('q'):
            break
        if key == ord('w'):
            #draw = False
            myPoints = []
        if key == ord('d'):
            draw = True
        if key == ord('s'):
            draw = False
        if key == ord('r'):
            if red == 255:
                red = 0
            else:
                red = 255
        if key == ord('g'):
            if green == 255:
                green = 0
            else:
                green = 255
        if key == ord('b'):
            if blue == 255:
                blue = 0
            else:
                blue = 255
            colour = (255, 0, 0)

    video.release()
    cv2.destroyAllWindows()

