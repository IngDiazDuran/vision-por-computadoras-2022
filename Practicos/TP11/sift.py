#! /usr/bin/env python
# -*- coding: utf-8 -*-
import math
import numpy as np
import cv2
MIN_MATCH_COUNT = 10

img1 = cv2.imread("normal.jpeg")    # Leemos la imagen 1
img2 = cv2.imread("oblicua.jpeg")   # Leemos la imagen 2
img_h,img_w = img1.shape[:2]

dscr = cv2.SIFT_create() # Inicializamos el detector y el descriptor
kp1, des1 = dscr.detectAndCompute(img1, None) # Encontramos los puntos clave y los descriptores con SIFT en la imagen 1
kp2, des2 = dscr.detectAndCompute(img2, None) # Encontramos los puntos clave y los descriptores con SIFT en la imagen 2

matcher = cv2.BFMatcher(cv2.NORM_L2)

matches = matcher.knnMatch(des1,des2,k=2)

# Guardamos los buenos matches usando el test de razón de Lowe

good = []
for m, n in matches:
    if m.distance < 0.7*n.distance :
        good.append(m)

if (len(good) > MIN_MATCH_COUNT) :
    src_pts = np.float32([kp1[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
    dst_pts = np.float32 ([kp2[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)

    H, mask = cv2.findHomography(dst_pts, src_pts, cv2.RANSAC, 5.0) # Computamos la homografía con RAN-SAC

wimg2 = cv2.warpPerspective(img2,H,(img_w,img_h)) # Aplicamos la transformación perspectiva H a la imagen 2

# Mezclamos ambas imágenes
alpha = 0.5
blend = np.array(wimg2*alpha + img1*(1-alpha), dtype=np.uint8)

cv2.imwrite('Superposicion.jpeg', blend)
cv2.imshow('Imagen',blend)
cv2.waitKey(0)

good = sorted(good, key = lambda x:x.distance) # Listar los matches buenos de mejor a peor

img3 = cv2.drawMatches(img1, kp1, img2, kp2, good[:100], None, flags=2) # Mostrar las imágenes paralelamente, con los primeros 100 descriptores

img3_h,img3_w = img3.shape[:2]
img3_h = math.ceil(img3_h*0.8)
img3_w = math.ceil(img3_w*0.8)
img3 = cv2.resize(img3, (img3_w, img3_h))

cv2.imwrite('Matches.jpeg', img3)
cv2.imshow('Matches',img3)
cv2.waitKey(0)
cv2.destroyAllWindows()

